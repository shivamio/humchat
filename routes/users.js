const express = require('express');
const router = express.Router();
const baseKey = require('../config/basekey');
const passport = require('passport');
const Authentication = require('../models/authentication');
const User = require('../models/user');
const MsgRegister = require('../models/messageRegister');
const LatestMessages = require('../models/latestMessages');
const Leads = require('../models/leads');
const UserContacts = require('../models/userContacts');
const Nexmo = require('nexmo');
const random = require('randomstring');
const jwt = require('jsonwebtoken')
const config = require('../config/database');

const nexmo = new Nexmo({
    apiKey: 'a297339a',
    apiSecret: 'f96b41e3',
});

router.post('/exsignin', (req,res,next)=>{
    const verifyId = req.body.verifyId;
    const secretAnswer = req.body.answer;
    const email = req.body.email;
    const apiKey = req.header('apiKey');
    if (apiKey == undefined || apiKey == "") {
        res.status(401).json({
            success: false,
            msg: 'Unauthorized',
            code: 124
        })
    } else {
        if (apiKey == baseKey.baseKey) {
            if(verifyId == undefined || verifyId == "" || email == undefined || email == "" || secretAnswer == undefined || secretAnswer == ""){
                res.status(400).json({
                    success: false,
                    msg: 'Incomplete request',
                    code: 125
                })  
            }else{
                Authentication.fetchVerification(verifyId, (err1, verifyObject) => {
                    if(err1){
                        res.status(500).json({
                            success:false,
                            msg:'Server error',
                            code:127
                        })
                    }if(verifyObject){
                        if(verifyObject.verified){
                            Authentication.removeVerificationRecord(verifyId, (x, y) => { });
                            User.getUserByPhone(verifyObject.phone, (err2, exUser)=>{
                                if(err2){
                                    res.status(500).json({
                                        success:false,
                                        msg:'Server error',
                                        code:130
                                    })
                                }if(exUser){
                                    const givenAnswer = secretAnswer.toLowerCase();
                                    const givenEmail = email.toLowerCase();
                                    const exEmail = exUser.email.toLowerCase();
                                    const exAnswer = exUser.secretAns.toLowerCase();
                                    if (exAnswer == givenAnswer && exEmail == givenEmail){
                                        const token = jwt.sign({ data: exUser }, config.secret, {});
                                        res.json({
                                            success:true,
                                            msg:'Verified',
                                            code:2,
                                            token:'Bearer '+token,
                                            socketToken:token,
                                            user:{
                                                id:exUser._id,
                                                name:exUser.name,
                                                phone:exUser.phone,
                                                dp:exUser.dp,
                                                status:exUser.status
                                            }
                                        })
                                    }else{
                                        res.json({
                                            success:false,
                                            msg:'Incorrect details provided',
                                            code:132
                                        })
                                    }
                                }else{
                                    res.status(400).json({
                                        success:false,
                                        msg:'User not found',
                                        code:131
                                    })
                                }
                            })
                        }else{
                            res.status(400).json({
                                success:false,
                                msg:'User not verified',
                                code:129
                            })
                        }
                    }else{
                        res.status(400).json({
                            success:false,
                            msg:'Invalid verifyId',
                            code:128
                        })
                    }
                })
            }
        }else{
            res.status(401).json({
                success: false,
                msg: 'Unauthorized',
                code: 126
            })
        }
    }
})

router.post('/freshsignin', (req,res,next)=>{
    const verifyId = req.body.verifyId;
    const apiKey = req.header('apiKey');
    const name = req.body.name;
    const email = req.body.email;
    const secretQues = req.body.secretQues;
    const secretAns = req.body.secretAns;
    const dpRefId = random.generate({length:'8', charset:'url-safe'});

    if (apiKey == undefined || apiKey == "") {
        res.status(401).json({
            success: false,
            msg: 'Unauthorized',
            code: 133
        })
    } else {
        if (apiKey == baseKey.baseKey) {
            if(verifyId == undefined || verifyId == "" || name == undefined || name == "" || email == undefined || email == "" || secretQues == undefined || secretQues == "" || secretAns == undefined || secretAns == ""){
                res.status(400).json({
                    success: false,
                    msg: 'Incomplete request',
                    code: 134
                });
            }else{
                Authentication.fetchVerification(verifyId, (err1, verifyObject)=>{
                    if(err1){
                        res.status(500).json({
                            success:false,
                            msg:'Server error',
                            code:136
                        })
                    }if(verifyObject){
                        if(verifyObject.verified){
                            Authentication.removeVerificationRecord(verifyId, (x, y) => { });
                            //Check if user already there. If yes, delete it
                            User.getUserByPhone(verifyObject.phone, (err2, already)=>{
                                if(err2){
                                    res.status(500).json({
                                        success:false,
                                        msg:'Server error',
                                        code:139
                                    })
                                }if(already){
                                    //delete and create new
                                    MsgRegister.deleteRegisterByUser(already._id, (a,b)=>{});
                                    LatestMessages.deleteLatestMessage(already._id, (d,f)=>{});
                                    UserContacts.removeContactBook(already._id, (z,s)=>{});
                                    Leads.removeAllLeadsOfUser(already._id, (k,l)=>{});
                                    User.removeUser(already._id, (err4, deleted)=>{
                                        if(err4){
                                            res.status(500).json({
                                                success:false,
                                                msg:'Server error',
                                                code:141
                                            })
                                        }else{
                                            const addUser = new User({
                                                name: name,
                                                phone: verifyObject.phone,
                                                email: email,
                                                secretQues: secretQues,
                                                secretAns: secretAns,
                                                dp:dpRefId
                                            })
                                            User.getUserByEmail(email, (err10, yesByEmail)=>{
                                                if(err10){
                                                    res.status(500).json({
                                                        success: false,
                                                        msg: 'Server error',
                                                        code: 172
                                                    })
                                                }if(yesByEmail){
                                                    res.status(500).json({
                                                        success: false,
                                                        msg: 'Email connected with other account',
                                                        code: 173
                                                    })
                                                }else{
                                                    User.newUser(addUser, (err5, done) => {
                                                        if (err5) {
                                                            res.status(500).json({
                                                                success: false,
                                                                msg: 'Server error',
                                                                code: 142
                                                            })
                                                        } if (done) {
                                                            const newSignal = random.generate({ length: 6, charset:'alphanumeric'})
                                                            const newUserId = done._id;
                                                            const registerObj = new MsgRegister({signal:newSignal, user:newUserId, register:[], latest:[]})
                                                            MsgRegister.newRegister(registerObj, (p,q)=>{});
                                                            LatestMessages.createLatestMessage(done._id, (i,j)=>{});
                                                            UserContacts.createContactBook(done._id, (f,g)=>{});
                                                            Leads.checkForLead(done.phone, (eo1, lo1)=>{
                                                                if(lo1.length >=1){
                                                                    lo1.forEach(ls => {
                                                                        req.app.io.emit(ls.user, { eventType: 'lead-converted', phone:ls.phone, name:ls.name});
                                                                    });
                                                                    Leads.removeLead(done._id, (y,u)=>{});
                                                                }
                                                            })
                                                            const token = jwt.sign({ data: done }, config.secret, {});
                                                            res.json({
                                                                success: true,
                                                                msg: 'Verified',
                                                                code: 4,
                                                                token: 'Bearer ' +token,
                                                                socketToken:token,
                                                                user: {
                                                                    id: done._id,
                                                                    name: done.name,
                                                                    phone: done.phone
                                                                }
                                                            })
                                                        }
                                                    })
                                                }
                                            })
                                        }
                                    })
                                }else{
                                    const addUser = new User({
                                        name:name,
                                        phone:verifyObject.phone,
                                        email:email,
                                        secretQues:secretQues,
                                        secretAns: secretAns,
                                        dp:dpRefId
                                    })
                                    User.getUserByEmail(email, (err10, yesByEmail)=>{
                                        if(err10){
                                            res.status(500).json({
                                                success: false,
                                                msg: 'Server error',
                                                code: 172
                                            })
                                        }if(yesByEmail){
                                            res.status(500).json({
                                                success: false,
                                                msg: 'Email connected with other account',
                                                code: 173
                                            })
                                        }else{
                                            User.newUser(addUser, (err5, done) => {
                                                if (err5) {
                                                    res.status(500).json({
                                                        success: false,
                                                        msg: 'Server error',
                                                        code: 142
                                                    })
                                                } if (done) {
                                                    const newSignal = random.generate({ length: 6, charset:'alphanumeric'})
                                                    const newUserId = done._id;
                                                    const registerObj = new MsgRegister({signal:newSignal, user:newUserId, register:[], latest:[]})
                                                    MsgRegister.newRegister(registerObj, (p,q)=>{});
                                                    LatestMessages.createLatestMessage(done._id, (i,j)=>{});
                                                    UserContacts.createContactBook(done._id, (f,g)=>{});
                                                    Leads.checkForLead(done.phone, (eo1, lo1)=>{
                                                        if(lo1.length >=1){
                                                            lo1.forEach(ls => {
                                                                req.app.io.emit(ls.user, { eventType: 'lead-converted', phone:ls.phone, name:ls.name});
                                                            });
                                                            Leads.removeLead(done._id, (y,u)=>{});
                                                        }
                                                    })
                                                    const token = jwt.sign({ data: done }, config.secret, {});
                                                    res.json({
                                                        success: true,
                                                        msg: 'Verified',
                                                        code: 4,
                                                        token: 'Bearer ' +token,
                                                        socketToken:token,
                                                        user: {
                                                            id: done._id,
                                                            name: done.name,
                                                            phone: done.phone
                                                        }
                                                    })
                                                }
                                            })
                                        }
                                    })
                                }
                            })
                        }else{
                            res.status(400).json({
                                success: false,
                                msg: 'User not verified',
                                code: 138
                            })
                        }
                    }else{
                        res.status(400).json({
                            success: false,
                            msg: 'Invalid verifyId',
                            code: 137
                        })
                    }
                })
            }
        }else{
            res.status(401).json({
                success: false,
                msg: 'Unauthorized',
                code: 135
            })
        }
    }
})

router.post('/verify', (req, res, next) =>{
    const reqId = req.body.reqId;
    const otp = req.body.otp;
    const verifyId = req.body.verifyId;

    const apiKey = req.header('apiKey');
    if (apiKey == undefined || apiKey == "") {
        res.status(401).json({
            success: false,
            msg: 'Unauthorized',
            code: 112
        })
    } else {
        if (apiKey == baseKey.baseKey) {
            if(reqId == undefined || reqId == "" || otp == undefined || otp == "" || verifyId ==  undefined || verifyId == ""){
                res.status(400).json({
                    success: false,
                    msg: 'Incomplete request',
                    code: 113
                })   
            }else{
                const isnum = /^\d+$/.test(otp);
                if(isnum){
                    if(otp.length == 4){
                        //fetch verification
                        Authentication.fetchVerification(verifyId, (err1, verifyObject) => {
                            if(err1){
                                res.status(500).json({
                                    success:false,
                                    msg:'Server error',
                                    code:117
                                })
                            }if(verifyObject){
                                console.log(verifyObject);
                                if(reqId == verifyObject.reqId){
                                    //now verify otp
                                    nexmo.verify.check({
                                        request_id: reqId,
                                        code: otp
                                    }, (verifyErr, verifyResult) => {
                                        if(verifyErr){
                                            res.status(500).json({
                                                success:false,
                                                msg:'Server error',
                                                code:120
                                            })
                                        }if(verifyResult){
                                            console.log(verifyResult);
                                            if(verifyResult.status == 0){
                                                //set verify true
                                                Authentication.setVerified(verifyObject._id, (err3, done)=>{
                                                    if(err3){
                                                        res.status(500).json({
                                                            success:false,
                                                            msg:'Server error',
                                                            code:123
                                                        })
                                                    }if(done){
                                                        //Now check if user already exists
                                                        User.getUserByPhone(verifyObject.phone, (err2, already) => {
                                                            if (err2) {
                                                                res.status(500).json({
                                                                    success: false,
                                                                    msg: 'Server error',
                                                                    code: 122
                                                                })
                                                            }
                                                            if (already) {
                                                                res.json({
                                                                    success:true,
                                                                    msg:'OTP verified',
                                                                    isNew:false,
                                                                    secretQues: already.secretQues,
                                                                    code:3
                                                                })
                                                            }else{
                                                                res.json({
                                                                    success: true,
                                                                    msg: 'OTP verified',
                                                                    isNew: true,
                                                                    code: 4
                                                                })
                                                            }
                                                        })
                                                    }
                                                })
                                            }else{
                                                res.json({
                                                    success:false,
                                                    msg:'Invalid otp',
                                                    code:121
                                                })
                                            }
                                        }
                                    });
                                }else{
                                    res.status(400).json({
                                        success: false,
                                        msg: 'Invalid reqId',
                                        code: 119
                                    })
                                }
                            }else{
                                res.status(400).json({
                                    success:false,
                                    msg:'Invalid verification id',
                                    code:118
                                })
                            }
                        })
                    }else{
                        res.status(400).json({
                            success: false,
                            msg: 'otp should be 4 digit',
                            code: 114
                        })
                    }
                }else{
                    res.status(400).json({
                        success: false,
                        msg: 'Invalid OTP',
                        code: 115
                    })
                }
            }
        }else{
            res.status(401).json({
                success: false,
                msg: 'Unauthorized',
                code: 116
            })
        }
    }
})


router.post('/beginauth', (req,res,next)=>{
    const apiKey = req.header('apiKey');
    if(apiKey == undefined || apiKey == ""){
        res.status(401).json({
            success:false,
            msg:'Unauthorized',
            code:100
        })
    }else{
        if (apiKey == baseKey.baseKey) {
            const phone = req.body.phone;
            const cCode = req.body.cCode;
            if(phone == undefined || phone == "" || cCode == undefined || cCode == ""){
                res.status(400).json({
                    success: false,
                    msg: 'Incomplete request',
                    code: 102
                })      
            }else{
                const isnum = /^\d+$/.test(phone);
                const isCodeNum = /^\d+$/.test(cCode);
                if(isnum && isCodeNum){
                    if(phone.length == 10){
                        Authentication.getVerificationByPhone(phone, (err, already)=>{
                            if(err){
                                res.status(500).json({
                                    success: false,
                                    msg: 'Server error',
                                    code: 104
                                })
                            }
                            if(already){
                                //User already tried. Check num of try's
                                const count = already.sentCount;
                                console.log(`count is ${count}`)
                                if(count > 3){
                                    res.status(400).json({
                                        success: false,
                                        msg: 'Too many requests, try after 24h',
                                        code: 108
                                    })
                                }else{
                                    //generate otp
                                    const newCount = count+1
                                    const otpToSend = random.generate({ length: 4, charset: 'numeric' });
                                    const time = new Date();
                                    Authentication.setNewTry(already._id, time, newCount, (err2, created) => {
                                        if (err2) {
                                            res.status(500).json({
                                                success: false,
                                                msg: 'Server error',
                                                code: 105
                                            })
                                        }
                                        if (created) {
                                        //Begin OTP Verification
                                            nexmo.verify.request({
                                                number: `${cCode}${phone}`,
                                                brand: 'HumChat',
                                                code_length: '4'
                                            }, (errNexmo, sendResult) => {
                                                if(errNexmo){
                                                    res.status(500).json({
                                                        success:false,
                                                        msg:'Server error',
                                                        code:109
                                                    })
                                                }if(sendResult){
                                                    console.log(sendResult)
                                                    if(sendResult.status == 0){
                                                        const reqId = sendResult.request_id;
                                                        Authentication.setReqId(already._id, reqId, (err3, refSaved)=>{
                                                            if(err3){
                                                                res.status(500).json({
                                                                    success: false,
                                                                    msg: 'Server error',
                                                                    code: 111
                                                                })
                                                            }if(refSaved){
                                                                res.json({
                                                                    success:true,
                                                                    msg:'OTP sent',
                                                                    reqId: refSaved.reqId,
                                                                    verifyId:already._id,
                                                                    code:1
                                                                })
                                                            }
                                                        })
                                                    }else{
                                                        res.status(500).json({
                                                            success: false,
                                                            msg: 'Server error',
                                                            code: 110
                                                        })
                                                    }
                                                }
                                            });

                                        } else {
                                            res.status(500).json({
                                                success: false,
                                                msg: 'Server error',
                                                code: 106
                                            })
                                        }
                                    })
                                }

                            }else{
                                //generate otp
                                //const otpToSend = random.generate({ length: 4, charset: 'numeric'});
                                const time = new Date();
                                let newAuth = new Authentication({
                                    phone:phone,
                                    sentCount:1,
                                    time:time
                                })
                                Authentication.newVerification(newAuth, (err2, created)=>{
                                    if(err2){
                                        res.status(500).json({
                                            success: false,
                                            msg: 'Server error',
                                            code: 105
                                        })
                                    }
                                    if(created){
                                        nexmo.verify.request({
                                            number: `${cCode}${phone}`,
                                            brand: 'HumChat',
                                            code_length: '4'
                                        }, (errNexmo, sendResult) => {
                                            if (errNexmo) {
                                                res.status(500).json({
                                                    success: false,
                                                    msg: 'Server error',
                                                    code: 109
                                                })
                                            } if (sendResult) {
                                                console.log(sendResult)
                                                if (sendResult.status == 0) {
                                                    const reqId = sendResult.request_id;
                                                    Authentication.setReqId(created._id, reqId, (err3, refSaved) => {
                                                        if (err3) {
                                                            res.status(500).json({
                                                                success: false,
                                                                msg: 'Server error',
                                                                code: 111
                                                            })
                                                        } if (refSaved) {
                                                            res.json({
                                                                success: true,
                                                                msg: 'OTP sent',
                                                                reqId: refSaved.reqId,
                                                                verifyId: created._id,
                                                                code:2
                                                            })
                                                        }
                                                    })
                                                } else {
                                                    res.status(500).json({
                                                        success: false,
                                                        msg: 'Server error',
                                                        code: 110
                                                    })
                                                }
                                            }
                                        });

                                    }else{
                                        res.status(500).json({
                                            success: false,
                                            msg: 'Server error',
                                            code: 106
                                        })
                                    }
                                })
                            }
                        })
                    }else{
                        res.status(400).json({
                            success: false,
                            msg: 'Phone number should be 10 digit',
                            code: 103
                        })    
                    }
                }else{
                    res.status(400).json({
                        success: false,
                        msg: 'Invalid phone number',
                        code: 104
                    })  
                }
            }

        } else {
            res.status(401).json({
                success: false,
                msg: 'Unauthorized',
                code: 101
            })  
        }
    }
})

router.get('/myprofile', passport.authenticate('jwt', { session: false }), (req,res,next)=>{
    const apiKey = req.header('apiKey');
    if (apiKey == undefined || apiKey == "") {
        res.status(401).json({
            success: false,
            msg: 'Unauthorized',
            code: 100
        })
    } else {
        if (apiKey == baseKey.baseKey) {
            res.json({
                success:true,
                msg:'My Profile',
                user:req.user
            })
        }
    }
})


module.exports = router;