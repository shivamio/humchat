const express = require('express');
const router = express.Router();
const baseKey = require('../config/basekey');
const passport = require('passport');
const Message = require('../models/userMessage');
const User = require('../models/user');
const MessageRegister = require('../models/messageRegister');
const LatestMessages = require('../models/latestMessages');
const dateTime = require('date-and-time');
const random = require('randomstring');

const textMsgCharLimit = 10000

router.post('/text', passport.authenticate('jwt', { session: false }), (req, res, next) =>{
    const from = req.user._id;
    const to = req.body.to;
    const text = req.body.text;
    const apiKey = req.header('apiKey');
    if (apiKey == undefined || apiKey == "") {
        res.status(401).json({
            success: false,
            msg: 'Unauthorized',
            code: 143
        })
    } else {
        if (apiKey == baseKey.baseKey) {
            if(to == undefined || to == "" || text == undefined || text == ""){
                res.status(400).json({
                    success:false,
                    msg:'Incomplete request',
                    code:147
                })
            }else{
                if(to.length == 24){
                    if (text.length <= textMsgCharLimit && text.length > 1){
                        //Check if recipient exists
                        User.getUserById(to, (err1, yes)=>{
                            if(err1){
                                res.status(500).json({
                                    success: false,
                                    msg: 'Server error',
                                    code: 148
                                })
                            }
                            if(yes){
                                if (yes.acountStatus){
                                    //TODO:implement blacklist check
                                    const now = new Date()
                                    const message = new Message({
                                        origin:from,
                                        from:from,
                                        to:yes._id,
                                        msgType:'text',
                                        text:text,
                                        timestamp:now
                                    })
                                    Message.newMessage(message, (err2, saved)=>{
                                        if(err2){
                                            res.status(500).json({
                                                success: false,
                                                msg: 'Server error',
                                                code: 148
                                            })
                                        }
                                        if(saved){
                                            //save in registers, latest messages and emit socket event
                                            const signal1 = random.generate({length: 6, charset: 'alphanumeric'})
                                            const signal2 = random.generate({ length: 6, charset: 'alphanumeric' })
                                            const msgId = saved._id;
                                            var short = ""
                                            if(text.length >= 20){
                                                short = text.substring(0, 19)
                                            }else{
                                                short = text
                                            }
                                            //first add sender's entry then receiver's entry
                                            MessageRegister.newEntry(from, signal1, msgId, (err3, entry1)=>{
                                                if(err3){
                                                    res.status(500).json({
                                                        success: false,
                                                        msg: 'Server error',
                                                        code: 148
                                                    })
                                                }else{
                                                    MessageRegister.newEntry(to, signal2, msgId, (err4, entry2)=>{
                                                        if(err4){
                                                            res.status(500).json({
                                                                success: false,
                                                                msg: 'Server error',
                                                                code: 148
                                                            })
                                                        }else{
                                                            //Now add to latest messages(sender first)
                                                            let latestObjForSender = {
                                                                other:to,
                                                                display:'me',
                                                                short:short,
                                                                phone:yes.phone,
                                                                timestamp:now
                                                            }
                                                            LatestMessages.addLatestMsg(from, latestObjForSender, (error, added)=>{
                                                                if(error){
                                                                    res.status(500).json({
                                                                        success: false,
                                                                        msg: 'Server error',
                                                                        code: 148
                                                                    })
                                                                }if(added){
                                                                    let latestObjForReceiver = {
                                                                        other: from,
                                                                        display: req.user.name,
                                                                        short: short,
                                                                        phone:req.user.phone,
                                                                        timestamp: now
                                                                    }
                                                                    LatestMessages.addLatestMsg(to, latestObjForReceiver, (error2, added2) =>{
                                                                        if(error2){
                                                                            res.status(500).json({
                                                                                success: false,
                                                                                msg: 'Server error',
                                                                                code: 148
                                                                            })
                                                                        }
                                                                        if(added2){
                                                                            Message.ackSender(msgId, (x, y) => { });
                                                                            //Notify both parties
                                                                            req.app.io.emit(from, { eventType: 'new-message', msgType: 'text', from: from, to: to, msgId: msgId, text: text });
                                                                            req.app.io.emit(to, { eventType: 'new-message', msgType: 'text', from: from, to: to, msgId: msgId, text: text });
                                                                            res.json({
                                                                                success:true,
                                                                                msg:'Message sent',
                                                                                msgId:msgId,
                                                                                code:5
                                                                            })
                                                                        }
                                                                    })
                                                                }
                                                            })
                                                        }
                                                    })
                                                }
                                            })
                                        }
                                    })
                                }else{
                                    res.status(400).json({
                                        success: false,
                                        msg: 'User disabled',
                                        code: 145
                                    })
                                }
                            }else{
                                res.status(400).json({
                                    success: false,
                                    msg: 'User not found',
                                    code: 145
                                })
                            }
                        })
                    }else{
                        res.status(400).json({
                            success: false,
                            msg: `Character limit exceeded ${textMsgCharLimit}`,
                            code:146
                        })
                    }
                }else{
                    res.status(400).json({
                        success: false,
                        msg: 'Invalid to field',
                        code:145
                    })
                }
            }
        }else{
            res.status(401).json({
                success: false,
                msg: 'Unauthorized',
                code: 144
            })
        }
    }
})


module.exports = router;