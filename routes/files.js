const express = require('express');
const router = express.Router();
const passport = require('passport');
const random = require('randomstring');
const multer = require('multer');
const File = require('../models/files');
const User = require('../models/user');
const path = require('path');
const fs = require('fs');

const hostName = 'http://localhost:3000'

var dpStorage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './ups/im/dps');
    },
    filename: function (req, file, cb) {
        var fileName = random.generate({ length: 30, type: 'url-safe' })
        if (file.mimetype == "image/jpeg") {
            cb(null, fileName + '.jpg');
        }
        if (file.mimetype == "image/png") {
            cb(null, fileName + '.png');
        }
    }
})
const dpUpload = multer({
    storage: dpStorage, fileFilter: function (req, file, cb) {
        const fileMime = file.mimetype;
        if (fileMime == 'image/jpeg' || fileMime == 'image/png') {
            cb(null, true)
        } else {
            cb('only images', false);
        }
    }
})
var dpUploads = dpUpload.single('dp');

router.post('/newdp', passport.authenticate('jwt', { session: false }), (req, res, next) => {
    dpUploads(req, res, function (err) {
        if(err) {
            console.log(err);
            res.status(500).json({
                success:false,
                msg:'Only images allowed'
            })
        }
        if(req.file){
            const fileId = req.user.dp;
            const now = new Date();
            let newFile = new File({
                fileId:fileId,
                dateOfUpload:now,
                fileMime:req.file.mimetype,
                uploadType:'dp',
                fileSize:req.file.size,
                user:req.user._id,
                localPath:`ups/im/dps/${req.file.filename}`,
                accessType:'public'
            })
            File.newDpFile(req.user._id, newFile, (err, other)=>{
                if(err){
                    res.json({
                        success:false,
                        msg:'Server error'
                    })
                }else{
                    res.json({
                        success:true,
                        msg:'DP uploaded'
                    })
                }
            })
        }
    })
})


router.get('/viewimage/:fileid',passport.authenticate('jwt', { session: false }), (req,res, next)=>{
    const fileId = req.params.fileid;
    const thisUser = req.user._id;
    File.fileStream(fileId, thisUser, (err, checked, canAccess, streamObj)=>{
        if(err){
            console.log(err);
            res.json({
                success:false,
                msg:'Server error'
            })
        }
        if(checked){
            if(canAccess){
                const lPath = streamObj.localPath;
                const absPath = path.resolve(__dirname+'/../'+lPath);
                res.sendFile(absPath);
            }else{
                res.json({
                    success:false,
                    msg:'Permission denied'
                })
            }
        }else{
            res.json({
                success:false,
                msg:'Failed to access'
            })
        }
    })
})


module.exports = router;