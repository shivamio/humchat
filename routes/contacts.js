const express = require('express');
const router = express.Router();
const UserContacts = require('../models/userContacts');
const Leads = require('../models/leads');
const baseKey = require('../config/basekey');
const passport = require('passport');

router.get('/checkfirstsync', passport.authenticate('jwt', { session: false }), (req, res, next) => {
    const user = req.user._id;
    const apiKey = req.header('apiKey');
    if (apiKey == undefined || apiKey == "") {
        res.status(401).json({
            success: false,
            msg: 'Unauthorized',
            code: 124
        })
    } else {
        if (apiKey == baseKey.baseKey) {
            UserContacts.checkFirstSync(user, (error, checked, isFirstDone)=>{
                if(error){
                    res.status(500).json({
                        success:false,
                        msg:'Server error',
                        code:170
                    })
                }
                if(checked){
                    res.json({
                        success:true,
                        msg:'Status checked',
                        firstSyncDone:isFirstDone,
                        code:11
                    })
                }else{
                    res.json({
                        success:false,
                        msg:'Failed to check',
                        code:171
                    })
                }
            })
        }else{
            res.status(401).json({
                success: false,
                msg: 'Unauthorized',
                code: 126
            })
        }
    }
})

router.post('/checkhum', passport.authenticate('jwt', { session: false }), (req, res, next) => {
    const user = req.user._id;
    const apiKey = req.header('apiKey');
    const numToTest = req.body.numToTest;
    if (apiKey == undefined || apiKey == "") {
        res.status(401).json({
            success: false,
            msg: 'Unauthorized',
            code: 124
        })
    } else {
        if (apiKey == baseKey.baseKey) {
            UserContacts.checkIfHumUser(numToTest, (err, checked, yes, humId)=>{
                if(err){
                    console.log(err);
                    res.json({
                        success:false,
                        msg:'Server error'
                    })
                }if(checked){
                    if(yes){
                        res.json({
                            success:true,
                            msg:'Its HumChat user',
                            isHum:true,
                            phone:numToTest,
                            humId:humId
                        })
                    }else{
                        res.json({
                            success:true,
                            isHum:false,
                            msg:'Not HumChat user'
                        })
                    }
                }else{
                    res.json({
                        success:false,
                        msg:'Failed to check'
                    })
                }
            })
        }else{
            res.status(401).json({
                success: false,
                msg: 'Unauthorized',
                code: 126
            })
        }
    }
})

router.post('/sync', passport.authenticate('jwt', { session: false }), (req, res, next) => {
    const user = req.user._id;
    const apiKey = req.header('apiKey');
    const localList = req.body.localList;
    if (apiKey == undefined || apiKey == "") {
        res.status(401).json({
            success: false,
            msg: 'Unauthorized',
            code: 124
        })
    } else {
        if (apiKey == baseKey.baseKey) {
            if(localList == undefined || localList == ""){
                res.status(401).json({
                    success: false,
                    msg: 'A Local list is required',
                    code: 172
                })
            }else{
                if(localList.length >=1){
                    UserContacts.removeDuplicatesFromLocalList(localList, (newList)=>{
                        totalContacts = newList.length;
                        newList.forEach(i => {
                            const numToCheck = i.phone;
                            const numName = i.name;
                            UserContacts.checkIfHumUser(numToCheck, (error, checked, ifYes, yesId)=>{
                                if(error) throw error;
                                if(checked){
                                    if(ifYes){
                                        console.log(numToCheck+' is hum user with id '+yesId);
                                        let newHumContact = {
                                            name:numName,
                                            phone:numToCheck,
                                            id:yesId
                                        }
                                        UserContacts.addContactToHumList(user, newHumContact, (e, added)=>{
                                            if(e) throw e;
                                            if(added){
                                                console.log('daal diya humlist mein');
                                            }
                                        })
                                    }else{
                                        console.log(numToCheck+' is not hum user');
                                        let newNoHumContact = {
                                            phone:numToCheck,
                                            name:numName
                                        }
                                        UserContacts.addContactToNoHumList(user, newNoHumContact, (e, added)=>{
                                            if(e) throw e;
                                            if(added){
                                                Leads.checkForLeadOfUserAndAdd(numToCheck, numName, user, (c,v)=>{});
                                                console.log('daal diya no-humlist mein');
                                            }
                                        })
                                    }
                                }
                            })
                        });
                        res.json({
                            msg:'ho gya'
                        })
                    })
                }else{
                    res.status(401).json({
                        success: false,
                        msg: 'A Local list is required',
                        code: 174
                    })
                }
            }
        }else{
            res.status(401).json({
                success: false,
                msg: 'Unauthorized',
                code: 126
            })
        }
    }
})


module.exports = router;