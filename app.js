const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const passport = require('passport');
const SocketAuth = require('./operations/socketAuth');
const SocketTexting = require('./operations/socketTexting');
const cors = require('cors')
const mongoose = require('mongoose');

const config = require('./config/database');

//Connect to mongoDB
mongoose.connect(config.database);
mongoose.connection.on('connected', () => {
    console.log('Connected to database ' + config.database);
})
mongoose.connection.on('error', (err) => {
    console.log('Database connection error ' + err);
})


const app = express();

//Socket.io stuff
var server = require('http').createServer(app);
var io = require('socket.io')(server);

const port = 3000;

app.use(cors());

app.use(bodyParser.json());
//Add Passport stuff here
app.use(passport.initialize());
app.use(passport.session());
require('./config/passport')(passport);


//Setup public folder here
app.use(express.static(path.join(__dirname, 'public')))

//setup routes here
const users = require('./routes/users');
const messages = require('./routes/message');
const files = require('./routes/files');
const contacts = require('./routes/contacts');


app.use('/users', users);
app.use('/message', messages);
app.use('/files', files);
app.use('/contacts', contacts);


app.get('/', (req, res, next) => {
    res.send("HumChat");
});

//Socket.io stuff
app.io = io;

io.on('connection', function (socket) {
    console.log('User connected');

    //Fetch latest messages list
    socket.on('fetch-latest-messages', (msgObj) => {
        const jwt = msgObj.jwt;
        const apiToken = msgObj.apiToken;
        SocketAuth.authenticateJwt(jwt, (err, uid) => {
            if (err) {
                console.log('Socket_Text_Message_Event: ' + 'Error:' + err);
            } if (uid) {
                SocketTexting.fetchLatestMessages(apiToken, uid, (fetchMsg, respCode, fetched, latestList) => {
                    if (fetched) {
                        socket.emit(uid, { eventType: 'latest-message-resp', success: true, msg: fetchMsg, code: respCode, latestList: latestList })
                    } else {
                        socket.emit(uid, { eventType: 'latest-message-resp', success: false, msg: fetchMsg, code: respCode })
                    }
                })
            } else {
                console.log('Socket_Text_Message_Event: ' + 'Unauthorized');
            }
        })

    })

    //Sending a text message via sockets
    socket.on('text-message', (msgObj) => {
        const jwt = msgObj.jwt;
        const apiToken = msgObj.apiToken;
        const to = msgObj.to;
        const text = msgObj.text;

        console.log('Socket_Text_Message_Event: ' + 'API_TOKEN:' + apiToken);
        SocketAuth.authenticateJwt(jwt, (err, from, phone) => {
            if (err) {
                console.log('Socket_Text_Message_Event: ' + 'Error:' + err);
            } if (from) {
                SocketTexting.sendText(to, from, phone, text, apiToken, (respMsg, sent, respCode, msgId) => {
                    if (sent) {
                        //Notify the receiver & sender of confirmation
                        socket.emit(from, { eventType: 'new-message', msgType: 'text', from: from, to: to, msgId: msgId, text: text });
                        socket.emit(to, { eventType: 'new-message', msgType: 'text', from: from, to: to, msgId: msgId, text: text });
                        //Notify the sender about success
                        socket.emit(from, { eventType: 'send-msg-response', success: true, msg: respMsg, code: respCode, msgId: msgId });
                    } else {
                        socket.emit(from, { eventType: 'send-msg-response', success: false, msg: respMsg, code: respCode });
                    }
                })
            } else {
                console.log('Socket_Text_Message_Event: ' + 'Unauthorized');
            }
        })
    })

    //Check for new messages (when woke from offline) via signals
    socket.on('check-new-messages', (msgObj) => {
        const jwt = msgObj.jwt;
        const apiToken = msgObj.apiToken;
        const mySignal = msgObj.mySignal;
        SocketAuth.authenticateJwt(jwt, (err, from, phone) => {
            if (err) {
                console.log('Socket_Text_Message_Event: ' + 'Error:' + err);
            } if (from) {
                SocketTexting.checkForNewMessages(apiToken, from, mySignal, (error, haveNew, msgList, serverSignal) => {
                    if (error) {
                        console.log('CheckNewMessageSocket: ' + error);
                        socket.emit(from, { eventType: 'check-new-messages-response', success: false, haveNew: null, serverSignal: null, msgList: null, msg: 'Error fetching new messages' });
                    } if (haveNew) {
                        socket.emit(from, { eventType: 'check-new-messages-response', success: true, haveNew: true, serverSignal: serverSignal, msgList: msgList, msg: 'New messages available, sync now' });
                    } else {
                        socket.emit(from, { eventType: 'check-new-messages-response', success: true, haveNew: false, serverSignal: serverSignal, msgList: null, msg: 'No new Messages to sync' });
                    }
                })
            } else {
                console.log('Socket_Text_Message_Event: ' + 'Unauthorized');
            }
        })
    })

    socket.on('get-this-message', (msgObj) => {
        const jwt = msgObj.jwt;
        const apiToken = msgObj.apiToken;
        const msgId = msgObj.msgId;
        SocketAuth.authenticateJwt(jwt, (err, from, phone) => {
            if (err) {
                console.log('Socket_Text_Message_Event: ' + 'Error:' + err);
            } if (from) {
                SocketTexting.fetchThisMessage(apiToken, from, msgId, (error, done, message) => {
                    if (error) {
                        socket.emit(from, { eventType: 'get-this-message-response', success: false, message: null, msg: error });
                    }
                    if (done) {
                        socket.emit(from, { eventType: 'get-this-message-response', success: true, msgId: msgId, message: message, msg: 'Message fetched' });
                        if (message.delivered != undefined) {
                            if (!message.delivered) {
                                socket.emit(message.from, { eventType: 'message-delivery-report', delivered: true, msgId: msgId, msg: 'Message delivered' });
                            }
                        }
                    }
                })
            } else {
                console.log('Socket_Text_Message_Event: ' + 'Unauthorized');
            }
        })
    })

    socket.on('get-conversation', (msgObj) => {
        const jwt = msgObj.jwt;
        const apiToken = msgObj.apiToken;
        const withUser = msgObj.withUser;
        SocketAuth.authenticateJwt(jwt, (err, from, phone) => {
            if (err) {
                console.log('Socket_Text_Message_Event: ' + 'Error:' + err);
            } if (from) {
                SocketTexting.fetchConversation(apiToken, from, withUser, (error, done, hasConversation, conversation) => {
                    if (error) {
                        socket.emit(from, { eventType: 'get-conversation-response', success: false, hasConversation: null, conversation: null, msg: error });
                    } if (done) {
                        socket.emit(from, { eventType: 'get-conversation-response', success: true, hasConversation: hasConversation, conversation: conversation, withUser: withUser, msg: 'Conversation fetched' });
                    }
                })
            } else {
                console.log('Socket_Text_Message_Event: ' + 'Unauthorized');
            }
        })
    })

    socket.on('ack-delivery', (msgObj) => {
        const jwt = msgObj.jwt;
        const apiToken = msgObj.apiToken;
        const msgList = msgObj.msgList;
        SocketAuth.authenticateJwt(jwt, (err, from, phone) => {
            if (err) {
                console.log('Socket_Text_Message_Event: ' + 'Error:' + err);
            } if (from) {
                if (msgList != undefined) {
                    if (msgList.length > 0) {
                        msgList.forEach(msgId => {
                            SocketTexting.ackDelivery(apiToken, msgId, (err, done, sender) => {
                                if (err) {
                                    console.log('ack-delivery-socket: error: ' + err);
                                } if (done) {
                                    socket.emit(sender, { eventType: 'message-delivery-report', delivered: true, msgId: msgId, msg: 'Message delivered' });
                                }
                            })
                        });
                    }
                }
            } else {
                console.log('Socket_Text_Message_Event: ' + 'Unauthorized');
            }
        })
    })

    socket.on('ack-read', (msgObj) => {
        const jwt = msgObj.jwt;
        const apiToken = msgObj.apiToken;
        const msgList = msgObj.msgList;
        SocketAuth.authenticateJwt(jwt, (err, from, phone) => {
            if (err) {
                console.log('Socket_Text_Message_Event: ' + 'Error:' + err);
            } if (from) {
                if (msgList != undefined) {
                    if (msgList.length > 0) {
                        msgList.forEach(msgId => {
                            SocketTexting.ackRead(apiToken, msgId, (err, done, sender) => {
                                if (err) {
                                    console.log('ack-read-socket: error: ' + err);
                                } if (done) {
                                    socket.emit(sender, { eventType: 'message-read-report', delivered: true, msgId: msgId, msg: 'Message read' });
                                }
                            })
                        });
                    }
                }
            } else {
                console.log('Socket_Text_Message_Event: ' + 'Unauthorized');
            }
        })
    })

});


server.listen(port, () => {
    console.log('Server started on port: ' + port)
});


