const mongoose = require('mongoose');
const UserContacts = require('./userContacts');

const FileStreamSchema = mongoose.Schema({
    fileId: {
        type: String,
        require: true
    },
    dateOfUpload:{
        type:String,
        required:true
    },
    fileMime: {
        type: String,
        required: true
    },
    uploadType:{
        type:String,
        required:true
    },
    fileSize: {
        type: String
    },
    user: {
        type: String,
        required: true
    },
    localPath: {
        type: String,
        required: true
    },
    accessType: {
        type: String,
        required: true
    },
    accessList: [
        { type: String }
    ]
});

const FileStream = module.exports = mongoose.model('FileStream', FileStreamSchema);

module.exports.newUpload = function (record, callback) {
    record.save(callback);
}

module.exports.newDpFile = function(user, fileObject, callback){
    FileStream.findOne({user:user, uploadType:'dp'}, (err, already)=>{
        if(err){
            return callback(err, false, false);
        }
        if(already){
            //update file
            //TODO: Implement deletion of previous file
            FileStream.findOneAndUpdate({user:user, uploadType:'dp'}, {dateOfUpload:fileObject.dateOfUpload, fileMime:fileObject.fileMime, fileSize:fileObject.fileSize, localPath:fileObject.localPath}, {new:true}, (err2, updated)=>{
                if(err2){
                    return callback(err2, false, false);
                }else{
                    return callback(true, true, true);
                }
            })
        }else{
            fileObject.save(callback);
        }
    })
}

module.exports.fileStream = function (id, user, callback) {
    const streamObj = {}
    FileStream.findOne({ fileId: id }, (err, fileRecord) => {
        if (err) {
            return callback('Server error', false, false, null);
        }
        if (fileRecord) {
            if (fileRecord.accessType == 'public') {
                streamObj.localPath = fileRecord.localPath;
                streamObj.fileMime = fileRecord.fileMime;
                streamObj.fileSize = fileRecord.fileSize;
                return callback(null, true, true, streamObj);
            } if (fileRecord.accessType == 'contacts') {
                //Check if the user is on contact list
                UserContacts.checkIfUserIsContact(fileRecord.user, user, (err2, checked, yes) => {
                    if (err2) {
                        return callback('Server error', false, false, null);
                    } if (checked) {
                        if (yes) {
                            streamObj.localPath = fileRecord.localPath;
                            streamObj.fileMime = fileRecord.fileMime;
                            streamObj.fileSize = fileRecord.fileSize;
                            return callback(null, true, true, streamObj);
                        } else {
                            return callback('Unauthorized', false, false, null);
                        }
                    }
                })
            }else{
                return callback('Unauthorized', false, false, null);
            }
        } else {
            return callback('Invalid file id', false, false, null);
        }
    })
}

module.exports.testStream = function(id, callback){
    FileStream.findOne({fileId:id}, callback);
}