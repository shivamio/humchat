const mongoose = require('mongoose');

const VerificationSchema = mongoose.Schema({
    phone:{
        type:String,
        required:true
    },
    sentOn:{
        type:String,
        required:true
    },
    tries:{
        type:Number,
        default:1
    },
    
})