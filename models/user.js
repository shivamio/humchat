const mongoose = require('mongoose');

const UserSchema = mongoose.Schema({
    name:{
        type:String,
        required:true
    },
    phone:{
        type:String,
        required:true
    },
    dp:{
        type:String
    },
    status:{
        type:String
    },
    email:{
        type:String,
        required:true
    },
    secretQues:{
        type:String,
        required:true
    },
    acountStatus:{
        type:Boolean,
        default:true
    },
    secretAns:{
        type:String,
        required:true
    }
})

const User = module.exports = mongoose.model('User', UserSchema);

module.exports.newUser = function(user, callback){
    user.save(callback);
}

module.exports.getUserById = function(id, callback){
    User.findById(id, callback);
}

module.exports.getUserByPhone = function(phone, callback){
    User.findOne({phone:phone}, callback);
}

module.exports.getUserByEmail = function(email, callback){
    User.findOne({email:email}, callback);
}

module.exports.newStatus = function(id, status, callback){
    User.findByIdAndUpdate(id, {status:status}, callback);
}

module.exports.newDP = function(id, dp, callback){
    User.findByIdAndUpdate(id, {dp:dp}, callback);
}

module.exports.removeUser = function(id, callback){
    User.findByIdAndRemove(id, callback);
}