const mongoose = require('mongoose');

const LeadsSchema = mongoose.Schema({
    leadPhone:{
        type:String,
        required:true
    },
    leadName:{
        type:String,
        required:true
    },
    user:{
        type:String,
        required:true
    }
});


const Leads = module.exports = mongoose.model('Leads', LeadsSchema);

module.exports.newLead = function(newLead, callback){
    newLead.save(callback);
}

//First step: After completing user registration. Check for leads to notify respective users
module.exports.checkForLead = function(phone, callback){
    Leads.find({leadPhone:phone}, callback);
}

//Second Step: When a lead becomes a HumChat user
module.exports.removeLead = function(phone, callback){
    Leads.remove({leadPhone:phone}, callback);
}

//Check if a lead already exist for a user before creating a new one
module.exports.checkForLeadOfUserAndAdd = function(phone, name, user, callback){
    Leads.findOne({leadPhone:phone, user:user}, (err, already)=>{
        if(err){
            return callback(err, false, false)
        }
        if(already){
            return callback(null, true, false)
        }else{
            let newLead = new Leads({
                leadPhone:phone,
                leadName:name,
                user:user
            })
            newLead.save(callback);
        }
    });
}

//When a user deletes account, delete its leads too
module.exports.removeAllLeadsOfUser = function(user, callback){
    Leads.remove({user:user}, callback);
}