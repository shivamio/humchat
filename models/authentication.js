const mongoose = require('mongoose');
const config = require('../config/basekey');

const VerificationSchema = mongoose.Schema({
    reqId:{
        type:String
    },
    phone:{
        type:String,
        required:true
    },
    sentCount:{
        type:Number,
        default:0
    },
    verified:{
        type:Boolean,
        default:false
    },
    time:{
        type:String,
        required:true
    }
});

const Verification = module.exports = mongoose.model('Verification', VerificationSchema);

module.exports.fetchVerification = function(id, callback){
    Verification.findById(id, callback);
}

module.exports.newVerification = function(verification, callback){
    verification.save(callback);
}

module.exports.setVerified = function(id, callback){
    Verification.findByIdAndUpdate(id, {verified:true}, callback);
}

module.exports.getVerificationByPhone = function(phone, callback){
    Verification.findOne({phone:phone}, callback);
}

module.exports.removeVerificationRecord = function(id, callback){
    Verification.findByIdAndRemove(id, callback);
}

module.exports.setNewTry = function(id, time, count, callback){
    Verification.findByIdAndUpdate(id, { sentCount: count, time:time}, callback);
}

module.exports.setReqId = function (id, reqId, callback){
    Verification.findByIdAndUpdate(id, {reqId: reqId}, {new:true}, callback);
}

module.exports.verifyUrn = function(urn, callback){
    if(urn == undefined || urn == ""){
        return callback(false);
    }else{
        if(urn.length == 16){
            const providedPrefix = urn.substring(0,9);
            if(providedPrefix == config.prefix){
                return callback(true)
            }else{
                return callback(false)
            }
        }else{
            return callback(false);
        }
    }
}