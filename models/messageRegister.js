const mongoose = require('mongoose');

const MessageRegisterSchema = mongoose.Schema({
    signal:{
        type:String,
        required:true
    },
    user:{
        type:String,
        required:true
    },
    register:[
        {
            type:String
        }
    ]
})

const MessageRegister = module.exports = mongoose.model('MessageRegister', MessageRegisterSchema);

module.exports.newRegister = function(newRegister, callback){
    newRegister.save(callback)
}

module.exports.deleteRegisterByUser = function(user, callback){
    MessageRegister.findOneAndRemove({user:user}, callback);
}

module.exports.findRegisterByUser = function(user, callback){
    MessageRegister.findOne({user:user}, callback);
}

module.exports.newEntry = function(user,signal, mid, callback){
    MessageRegister.findOneAndUpdate({ user: user }, { $addToSet:{register:mid}, signal:signal}, callback);
}

module.exports.emptyRegister = function(user, callback){
    MessageRegister.findOneAndUpdate({user:user}, {register:[]}, callback);
}

module.exports.checkForNewMessages = function(user, mySignal, callback){
    if(mySignal == undefined || mySignal == ""){
        return callback('Client signal required', null, null, null);
    }else{
        MessageRegister.findOne({ user: user }, (err, register) => {
            if (err) {
                return callback(err, null, null, null);
            } if (register) {
                const storedSignal = register.signal;
                if (storedSignal == undefined || storedSignal == "") {
                    return callback('Signal lost error', null, null, null)
                } else {
                    if (storedSignal == mySignal) {
                        //Everything is up-to-date, No need to fetch new messages
                        return callback(null, false, null, storedSignal)//(error, haveNew, messageList, serverSignal)
                    } else {
                        //There are new messages available
                        //Send the list of new messages and empty register for new messages to come
                        const newMsgList = register.register;
                        MessageRegister.findOneAndUpdate({ user: user }, { register: [] }, (x, y) => { });
                        return callback(null, true, newMsgList, storedSignal);
                    }
                }
            } else {
                return callback('Invalid user', null, null)
            }
        })
    }
}

module.exports.removeEntryFromRegister = function(user, msgId, callback){
    MessageRegister.findOneAndUpdate({user:user}, {$pull:{register:msgId}}, callback);
}