const mongoose = require('mongoose');
const User = require('../models/user');

const UserContactsSchema = mongoose.Schema({
    user:{
        type:String,
        required:true
    },
    firstSyncDone:{
        type:Boolean,
        default:false
    },
    lastSync:{
        type:String,
        required:true
    },
    humContacts:[
        {
            name:String,
            phone:String,
            dp:String,
            id:String
        }
    ],
    noHumContacts:[
        {
            name:String,
            phone:String,
        }
    ]
});

const UserContacts = module.exports = mongoose.model('UserContacts', UserContactsSchema);

module.exports.createContactBook = function(id, callback){
    const now = new Date();
    let newBook = new UserContacts({
        user:id,
        lastSync:now,
        humContacts:[],
        noHumContacts:[]
    })
    newBook.save(callback);
}

module.exports.fetchContactBook = function(id, callback){
    UserContacts.findOne({user:id}, callback);
}

module.exports.removeContactBook = function(id, callback){
    UserContacts.findOneAndDelete({user:id}, callback);
}

module.exports.checkFirstSync = function(id, callback){
    UserContacts.findOne({user:id}, (err, record)=>{
        if(err){
            return callback(err, false, false);//error, processComplete, firstSyncStatus
        }if(record){
            if(record.firstSyncDone){
                return callback(null, true, true);
            }else{
                return callback(null, true, false);
            }
        }else{
            return callback('Invalid user id', false, false);
        }
    })
}

module.exports.checkIfHumUser = function(phone, callback){
    User.getUserByPhone(phone, (err, user)=>{
        if(err){
            return callback(err, false, false, null);//error, processComplete, ifHumUser, userId
        }if(user){
            return callback(null, true, true, user._id);
        }else{
            return callback(null, true, false, null);
        }
    })
}

module.exports.syncContacts = function(userId, localUncheckedList, callback){
    const now = new Date();
    console.log(localUncheckedList);
    var localList = removeDuplicatesFromLocalList(localUncheckedList);
    console.log(localList);
    const numToCheck = localList.length;
    var humList = [];
    var noHumList = []
    var numsChecked = 0;
    localList.forEach(lCon=>{
        var thisPhone = lCon.phone;
        console.log('running createHumListFromLocalListFun: Testing number'+thisPhone);
        User.getUserByPhone(thisPhone, (err, yes)=>{
            if(err){throw err;}
            if(yes){
                console.log('running createHumListFromLocalListFun: Its HumChat user');
                let humContact = {
                    name:lCon.name,
                    phone:thisPhone,
                    dp:yes.dp,
                    id:yes._id
                }
                humList.push(humContact);
            }else{
                let noHumContact = {
                    name:lCon.name,
                    phone:thisPhone
                }
                noHumList.push(noHumContact);
            }
        })
        numsChecked++;
    })
    if(numsChecked == numToCheck){
        UserContacts.findOneAndUpdate({user:userId}, {firstSyncDone:true, lastSync:now, humContacts:humList, noHumContacts:noHumList}, {new:true}, callback);
    }
}

module.exports.removeDuplicatesFromLocalList = function(list, callback){
    list.forEach(function(i, u){
        var phoneA = i.phone;
        var duplicateCount = 0;
        list.forEach(y=>{
            if(phoneA == y.phone){
                duplicateCount++;
            }
        });
        if(duplicateCount > 1){
            list.splice(u,1);
        }
    });
    return callback(list);
}

module.exports.resetContactList = function(id, callback){
    UserContacts.findOneAndUpdate({user:id}, {humContacts:[], noHumContacts:[]}, {new:true}, callback);
}

module.exports.addContactToHumList = function(id, contact, callback){
    UserContacts.findOneAndUpdate({user:id, 'humContacts.phone':{'$ne':contact.phone}}, {$push:{humContacts:contact}}, {new:true}, callback);
}

module.exports.addContactToNoHumList = function(id, contact, callback){
    UserContacts.findOneAndUpdate({user:id, 'noHumContacts.phone':{'$ne':contact.phone}}, {$push:{noHumContacts:contact}}, {new:true}, callback);
}

function createHumListFromLocalList(list){
    console.log('running createHumListFromLocalListFun');
    var noHum=[]
    var humList = []
    list.forEach(lCon=>{
        var thisPhone = lCon.phone;
        console.log('running createHumListFromLocalListFun: Testing number'+thisPhone);
        User.getUserByPhone(thisPhone, (err, yes)=>{
            if(yes){
                console.log('running createHumListFromLocalListFun: Its HumChat user');
                let humContact = {
                    name:lCon.name,
                    phone:thisPhone,
                    dp:yes.dp,
                    id:yes._id
                }
                humList.push(humContact);
            }
        })
    })
    // console.log(humList);
    // const contactsObject = {
    //     humList:humList,
    //     noHumList:noHum
    // }
    return humList;
}

module.exports.checkIfUserIsContact = function(id, user, callback){
    UserContacts.findOne({user:id}, (err, cbook)=>{
        if(err){
            return callback('Server error', false, null);
        }if(cbook){
            var isContact = false;
            const theHumList = cbook.humContact;
            theHumList.forEach(i=>{
                if(i.id == user){
                    isContact = true;
                }
            })
            return callback(null, true, isContact);
        }else{
            return callback('Invalid user id', false, null)
        }
    })
}
