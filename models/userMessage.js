const mongoose = require('mongoose');

const UserMessageSchema = mongoose.Schema({
    origin:{
        type:String,
        required:true
    },
    from:{
        type:String,
        required:true
    },
    to:{
        type:String,
        required:true
    },
    msgType:{
        type:String,
        required:true
    },
    text:{
        type:String
    },
    audio:{
        type:String
    },
    video:{
        type:String
    },
    image:{
        type:String
    },
    location:{
        type:String
    },
    document:{
        type:String
    },
    timestamp:{
        type:String,
        required:true
    },
    delivered:{
        type:Boolean,
        default:false
    },
    read:{
        type:Boolean,
        default:false
    },
    senderAck:{
        type:Boolean,
        default:false
    },
    fwdCount:{
        type:Number,
        default:0
    }
})

const UserMessage = module.exports = mongoose.model('UserMessage', UserMessageSchema);

module.exports.newMessage = function(newMessage, callback){
    newMessage.save(callback);
}

module.exports.fetchConversation = function(user1, user2, callback){
    UserMessage.find({$or:[{$and:[{to:user1},{from:user2}]}, {$and:[{to:user2}, {from:user1}]}]}, null, {sort:{_id:-1}}, callback);
}

module.exports.fetchMsgById = function(id, callback){
    UserMessage.findById(id, callback);
}

module.exports.ackSender = function(id, callback){
    UserMessage.findByIdAndUpdate(id, {senderAck:true}, callback)
}

module.exports.setDelivered = function(id, callback){
    UserMessage.findByIdAndUpdate(id, {delivered:true}, {new:true}, callback)
}

module.exports.setRead = function(id, callback){
    UserMessage.findByIdAndUpdate(id, {read:true}, callback)
}