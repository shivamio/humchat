const mongoose = require('mongoose');

const LatestMessagesSchema = mongoose.Schema({
    user:{
        type:String,
        required:true
    },
    latest:[
        {
            other: String,
            display: String,
            short: String,
            phone:String,
            timestamp: String
        }
    ]
});

const LatestMessages = module.exports = mongoose.model('LatestMessages', LatestMessagesSchema);

module.exports.createLatestMessage = function(id, callback){
    let newLatestObject = new LatestMessages({
        user:id,
        latest:[]
    })
    newLatestObject.save(callback)
}

module.exports.deleteLatestMessage = function(id, callback){
    LatestMessages.findOneAndDelete({user:id}, callback)
}

module.exports.fetchLatestMessages = function(id, callback){
    LatestMessages.findOne({user:id}, callback);
}

module.exports.addLatestMsg = function(id, latestObj, callback){
    var hasAlready = false;
    const otherUser = latestObj.other;
    LatestMessages.findOne({user:id}, (err, data)=>{
        if(err){
            return callback(err, false);
        }
        if(data){
            console.log('addLatestMsg event: '+'Fetched latest messages')
            const latestMessages = data.latest;
            console.log('addLatestMsg event: ' + latestMessages)
            latestMessages.forEach(item => {
                if(item.other == otherUser){
                    hasAlready = true
                }
            });
            if(hasAlready){
                console.log('addLatestMsg event: ' + 'A message was already present, deleting it and adding new')
                LatestMessages.findOneAndUpdate({user:id}, {$pull:{latest:{other:otherUser}}}, (err4, done4)=>{
                    if(err4){
                        return callback(err4, false)
                    }if(done4){
                        console.log('addLatestMsg event: ' + 'Deleted successfully')
                        LatestMessages.findOneAndUpdate({user:id}, {$push:{latest:{$each:[latestObj], $sort:{timestamp:-1}}}}, (err5, done5)=>{
                            if(err5){
                                return callback(err5, false)
                            }if(done5){
                                console.log('addLatestMsg event: ' + 'Added new successfully')
                                return callback(null, true)
                            }else{
                                console.log('addLatestMsg event: ' + 'Deleted old but failed to add new')
                                return callback(null, false);
                            }
                        })
                    }else{
                        console.log('addLatestMsg event: ' + 'Failed to Deleted')
                        return callback(null, false)
                    }
                })
            }else{
                console.log('addLatestMsg event: ' + 'No previous message found or failed to check maybe, adding a new one')
                LatestMessages.findOneAndUpdate({ user: id }, { $push: { latest: { $each: [latestObj], $sort: { timestamp: -1 } } }}, (err3, done3)=>{
                    if(err3){
                        return callback(err3, false)
                    }if(done3){
                        console.log('addLatestMsg event: ' + 'done')
                        return callback(null, true)
                    }else{
                        console.log('addLatestMsg event: ' + 'Failed to add new message')
                        return callback(null, false);
                    }
                })
            }
        }else{
            console.log('addLatestMsg event: ' + 'Failed to fetch latest messages')
            return callback(null, false)
        }
    })
}