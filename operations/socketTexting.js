const baseKey = require('../config/basekey');
const Message = require('../models/userMessage');
const User = require('../models/user');
const MessageRegister = require('../models/messageRegister');
const LatestMessages = require('../models/latestMessages');
const dateTime = require('date-and-time');
const random = require('randomstring');

const textMsgCharLimit = 10000

module.exports.fetchLatestMessages = function(apikey, user, callback){
    if(apikey == undefined || apikey == ""){
        return callback("Key required", false, 156, null);
    }else{
        if(apikey == baseKey.baseKey){
            LatestMessages.fetchLatestMessages(user, (err, data)=>{
                if(err){
                    console.log(err);
                    return callback("Server error", false, 156, null);
                }if(data){
                    const newList = data.latest;
                    return callback("Latest messages fetched", true, 6, newList);
                }else{
                    return callback("Failed to fetch", false, 156, null);
                }
            })
        }else{
            return callback("Unauthorized", false, 157, null);
        }
    }
}

module.exports.sendText = function(to, from, fromPhone, text, apikey, callback){
    if(apikey == undefined || apikey == ""){
        return callback("Key required", false, 143, null);//callback(sendMsg, sentResult, responseCode, msgId)
    }else{
        if(apikey == baseKey.baseKey){
            if(to == undefined || to == "" || from == undefined || from == "" || text == undefined || text == ""){
                return callback("Incomplete request", false, 144, null);
            }else{
                if(to.length == 24){
                    if(text.length > 0 && text.length < textMsgCharLimit){
                        //Check if recipient exists
                        User.getUserById(to, (err1, yes) => {
                            if (err1) {
                                return callback("Server error", false, 145, null);
                            }
                            if (yes) {
                                if (yes.acountStatus) {
                                    //TODO:implement blacklist check
                                    const now = new Date()
                                    const message = new Message({
                                        origin: from,
                                        from: from,
                                        to: yes._id,
                                        msgType: 'text',
                                        text: text,
                                        timestamp: now
                                    })
                                    Message.newMessage(message, (err2, saved) => {
                                        if (err2) {
                                            return callback("Server error", false, 146, null);
                                        }
                                        if (saved) {
                                            //save in registers, latest messages and emit socket event
                                            const signal1 = random.generate({ length: 6, charset: 'alphanumeric' })
                                            const signal2 = random.generate({ length: 6, charset: 'alphanumeric' })
                                            const msgId = saved._id;
                                            var short = ""
                                            if (text.length >= 11) {
                                                short = text.substring(0, 11)
                                            } else {
                                                short = text
                                            }
                                            //first add sender's entry then receiver's entry
                                            MessageRegister.newEntry(from, signal1, msgId, (err3, entry1) => {
                                                if (err3) {
                                                    return callback("Server error", false, 147, null);
                                                } else {
                                                    MessageRegister.newEntry(to, signal2, msgId, (err4, entry2) => {
                                                        if (err4) {
                                                            return callback("Server error", false, 148, null);
                                                        } else {
                                                            //Now add to latest messages(sender first)
                                                            let latestObjForSender = {
                                                                other: to,
                                                                display: 'me',
                                                                short: short,
                                                                phone:yes.phone,
                                                                timestamp: now
                                                            }
                                                            LatestMessages.addLatestMsg(from, latestObjForSender, (error, added) => {
                                                                if (error) {
                                                                    return callback("Server error", false, 149, null);
                                                                } if (added) {
                                                                    let latestObjForReceiver = {
                                                                        other: from,
                                                                        display: from,
                                                                        short: short,
                                                                        phone: fromPhone,
                                                                        timestamp: now
                                                                    }
                                                                    LatestMessages.addLatestMsg(to, latestObjForReceiver, (error2, added2) => {
                                                                        if (error2) {
                                                                            return callback("Server error", false, 150, null);
                                                                        }
                                                                        if (added2) {
                                                                            Message.ackSender(msgId, (x,y)=>{});
                                                                            return callback("Message sent", true, 5, msgId);
                                                                        }
                                                                    })
                                                                }
                                                            })
                                                        }
                                                    })
                                                }
                                            })
                                        }
                                    })
                                } else {
                                    return callback("User account disabled", false, 151, null);
                                }
                            } else {
                                return callback("User not found", false, 152, null);
                            }
                        })
                    }else{
                        return callback("Text limit unsatisfied", false, 153, null);
                    }
                }else{
                    return callback("Invalid recepient id", false, 154, null);
                }
            }
        }else{
            return callback("Unauthorized", false, 155, null);
        }
    }
}

module.exports.sendImage = function(to, from, fromPhone, text, apikey, callback){
    if(apikey == undefined || apikey == ""){
        return callback("Key required", false, 143, null);//callback(sendMsg, sentResult, responseCode, msgId)
    }else{
        if(apikey == baseKey.baseKey){
            if(to == undefined || to == "" || from == undefined || from == "" || text == undefined || text == ""){
                return callback("Incomplete request", false, 144, null);
            }else{
                if(to.length == 24){
                    if(text.length > 0 && text.length < textMsgCharLimit){
                        //Check if recipient exists
                        User.getUserById(to, (err1, yes) => {
                            if (err1) {
                                return callback("Server error", false, 145, null);
                            }
                            if (yes) {
                                if (yes.acountStatus) {
                                    //TODO:implement blacklist check
                                   
                                } else {
                                    return callback("User account disabled", false, 151, null);
                                }
                            } else {
                                return callback("User not found", false, 152, null);
                            }
                        })
                        
                    }else{
                        return callback("Text limit unsatisfied", false, 153, null);
                    }
                }else{
                    return callback("Invalid recepient id", false, 154, null);
                }
            }
        }else{
            return callback("Unauthorized", false, 155, null);
        }
    }
}



module.exports.checkForNewMessages = function(token, user, signal, callback){
    if(token == undefined || token == ""){
        return callback('Unauthorized', null, null, null)
    }else{
        if(token == baseKey.baseKey){
            MessageRegister.checkForNewMessages(user, signal, (error, haveNew, messageList, serverSignal) => {
                if (error) {
                    console.log('checkForNewMessages: Error: ' + error);
                    return callback('Server error', null, null, null)
                } if (haveNew) {
                    return callback(null, true, messageList, serverSignal)
                } else {
                    return callback(null, false, null, serverSignal)
                }
            })
        }else{
            return callback('Unauthorized', null, null, null)
        }
    }
}

module.exports.fetchThisMessage = function(token, user, msgId, callback){
    if(token == undefined || token == ""){
        return callback('Unauthorized', false, null);
    }else{
        if(token == baseKey.baseKey){
            if(msgId == undefined || msgId == ""){
                return callback('Invalid msg id', false, null);
            }else{
                Message.fetchMsgById(msgId, (err, message) => {
                    if(err){
                        console.log('fetchThisMessage: Error: '+err);
                        return callback('Server error', false, null);
                    }
                    if(message){
                        if(message.from == user || message.to == user){
                            //Remove message from register
                            MessageRegister.findRegisterByUser(user, (e, register)=>{
                                let msgRegister = register.register;
                                msgRegister.forEach(item=>{
                                    if(item == msgId){
                                        MessageRegister.removeEntryFromRegister(user, msgId, (x, y) => { });
                                        Message.setDelivered(msgId, (a,b)=>{})
                                    }
                                })
                            })
                            return callback(null, true, message);
                        }else{
                            return callback('Action prohibited', false, null);
                        }
                    }else{
                        return callback('Invalid msg id', false, null);
                    }
                })
            }
        }else{
            return callback('Unauthorized', false, null)
        }

    }
}

module.exports.fetchConversation = function(token, user, otherUser, callback){
    if(token == undefined || token == ""){
        return callback('Unauthorized', false, null, null);
    }else{
        if(token == baseKey.baseKey){
            if(otherUser == undefined || otherUser == ""){
                return callback('Invalid other user', false, null,  null);
            }else{
                if(otherUser.length == 24){
                    User.getUserById(otherUser, (err, userExist)=>{
                        if(err){
                            return callback('Server error', false, null, null);
                        }
                        if(userExist){
                            Message.fetchConversation(user, otherUser, (err, conversation) => {
                                if (err) {
                                    return callback('Server error', false, null, null);
                                } if (conversation) {
                                    //Remove message(s) from register of requester
                                    //fetch register
                                    MessageRegister.findRegisterByUser(user, (e, register)=>{
                                        let msgRegister = register.register;
                                        conversation.forEach(message => {
                                            msgRegister.forEach(registerItem => {
                                                if(registerItem == message._id){
                                                    MessageRegister.removeEntryFromRegister(user, registerItem, (x, y) => { });
                                                    Message.setDelivered(registerItem, (a, b) => { })
                                                }
                                            })
                                        })  
                                    })
                                    return callback(null, true, true, conversation);
                                } else {
                                    return callback(null, true, false, null);
                                }
                            })
                        }else{
                            return callback('Other user not found', false, null, null);
                        }
                    })
                }else{
                    return callback('Invalid other user', false, null, null);
                }
            }
        }else{
            return callback('Unauthorized', false, null, null);
        }
    }
}

module.exports.ackDelivery = function(token, id, callback){
    if (token == undefined || token == "") {
        return callback('Unauthorized', false, null);
    } else {
        if (token == baseKey.baseKey) {
            if (id == undefined || id == "") {
                return callback('Invalid request', false, null);
            } else {
                Message.setDelivered(id, (err, done)=>{
                    if(err){
                        return callback('Server error', false, null);
                    }if(done){
                        MessageRegister.removeEntryFromRegister(done.to, done._id, (x, y) => { });
                        return callback(null, true, done.from);
                    }else{
                        return callback('Failed to acknowledge', false, null);
                    }
                })
            }
        } else {
            return callback('Unauthorized', false, null);
        }
    }
}

module.exports.ackRead = function (token, id, callback) {
    if (token == undefined || token == "") {
        return callback('Unauthorized', false, null);
    } else {
        if (token == baseKey.baseKey) {
            if (id == undefined || id == "") {
                return callback('Invalid request', false, null);
            } else {
                Message.setRead(id, (err, done) => {
                    if (err) {
                        return callback('Server error', false, null);
                    } if (done) {
                        return callback(null, true, done.from);
                    } else {
                        return callback('Failed to acknowledge', false, null);
                    }
                })
            }
        } else {
            return callback('Unauthorized', false, null);
        }
    }
}
