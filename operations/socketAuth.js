const User = require('../models/user');
const Authentication = require('../models/authentication');
const baseKey = require('../config/basekey');
const config = require('../config/database');
const jsonWebToken = require('jsonwebtoken');

module.exports.authenticateJwt = function(jwt, callback){
    jsonWebToken.verify(jwt, config.secret, (e, data)=>{
        if(e){
            return callback('Server error', null)
        }if(data){
            const uid = data.data._id;
            if(uid == null || uid == ""){
                return callback("Invalid token", null)
            }else{
                User.getUserById(uid, (err, user) => {
                    if (err) {
                        return callback('Server error', null);
                    }
                    if (user) {
                        if (user.acountStatus){
                            return callback(null, user._id, user.phone);
                        }else{
                            return callback('User account not active', null);
                        }
                    } else {
                        return callback('Invalid user', null);
                    }
                })
            }
        }else{
            return callback("Invalid token", null)
        }
    })
}